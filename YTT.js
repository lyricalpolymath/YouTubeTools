/**
YTT - YouTube Tools
2016.06.20 - by @lyricalpolymath
page: https://lyricalpolymath.gitlab.io/YouTubeTools
repo: https://gitlab.com/lyricalpolymath/YouTubeTools

Licence: 
CreativeCommons BY-NC-SA (Attribution-NonCommercial-ShareAlike 4.0 International)
https://creativecommons.org/licenses/by-nc-sa/4.0/
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode
*/   

//for some reason when you load the script it says "Uncaught ReferenceError: $ is not defined" - this offers another option to access the player  
//var videoObj = ( $ != undefined ) ? $("video") : yt.player.utils.VideoTagPool.instance.A[0]; 

//var videoObj = yt.player.utils.VideoTagPool.instance.A[0]; 
var videoObj = document.getElementsByTagName("video")[0];

(function(video) {
    
	//console.log("video: ", video);
	//console.log("video.playbackRate: " + video.playbackRate);
     
    ///////////////////////// SETTINGS
	var yttVersion = "20170314.2000";
	var speedViewDuration = 1500; 			// how long the speed feedback stays visible
	var speedMax = 120;                     // maximum rate speed


    //////////////////////// private vars
	var v = video;   
	var durationString;// = getVideoDurationString();
	
	var currentSpeed, 
		speedContainer, 
		speedContainerTimer;
   	            
	
	///////////////////////////////////////////////   INITIALIZATION
	function _initYTT() {
	   showConsoleInstructions(); 
	   getVideoDurationString(); 
	   initShortCuts(); 
	}
	
	// TODO do I even need this if I'm not going to write it to the video text
	// first time you start it capture the formatted text of the video 
	function getVideoDurationString() {
		if (durationString == "" ) durationString = document.getElementsByClassName("ytp-time-duration")[0].innerHTML;
		return durationString;
	} 
	//*/
	

	///////////////////////////////////////////////   SPEED RATE MODIFIERS	   

	function setVideoSpeed (speed) { 
	     console.log("video speed: " + speed );
	     v.playbackRate = speed;
		 updateSpeedView();	// launch the visual feedback
	}
	
	function getVideoSpeed() { 
		return v.playbackRate;
	}

	function changeSpeed (direction) { 
		var min = 0.25;
		var max = speedMax;

		// on the lower speeds you want to increate little by little but above a certain speed you want to increase faster     
		var increment;
		var currSpeed = v.playbackRate;
		if 		(currSpeed < 2) 					increment = 0.25;  // for speeds < 2
		else if (currSpeed >= 2 && currSpeed < 5) 	increment = 0.5;   // 2 < speeds < 5
		else 	                                    increment = 10;    // 5 < speeds < max 

		// define the direction with the multiplier
		currSpeed += (increment * direction)

		// never go above the maximum or below the minimum
		if (currSpeed < min ) currSpeed = min;
		if (currSpeed > max)  currSpeed = max; 


		//set the speed
		setVideoSpeed(currSpeed)		
	}                           

	function speedUp () 	{ changeSpeed(1) }
	function speedDown () 	{ changeSpeed(-1) } 
	function speedNormal()  { setVideoSpeed(1)} 
        


    ///////////////////////////////////////////////   SHORTCUTS

	function shortCutListener (e) { 
	    // console.log("shortCutListner e: ", e )
		// console.log("\n------------------ char detected: \n charCode: \t" + e.charCode + "\n key: \t" + e.key + "\n code: \t" + e.code + "\n shiftKey: \t" + e.shiftKey + "\n------------------------------------")	 

		// SPEED RATE SHORTCUTS
		if 		(e.key == ">") speedUp()      // <  charCode: 62
		else if (e.key == "<") speedDown()    // >  charCode: 60
		else if (e.key == "n") speedNormal()  // n  charCode: 110
	}    

	function initShortCuts () { 
		console.log("initShortCuts")
		document.addEventListener('keypress', shortCutListener);
	}

	function removeShortCuts () { 
	   document.removeEventListener('keypress', shortCutListener);
	}
     

    
	///////////////////////////////////////////////   VIEWS
	 
	// displays a feedback with instructions when it is successfully loaded in the console
	function showConsoleInstructions() {
		console.log("\n------------------------------------------------------------\n"); 
		console.log("%cYouTube.Tools", "background-color:#e62117; font-size:3em; color:white; padding: 5px 20px 5px 20px; margin-bottom: 20px")
		console.log("\nversion: " + yttVersion);
	  	console.log("repository https://gitlab.com/lyricalpolymath/YouTubeTools");
		console.log("\n------------------------------------------------------------\n");
		
		console.log("Shortcuts")
		console.log("IMPORTANT: for the shortcuts to work don't click or focus on the video, click anywhere else on the page")
		console.log("speed up:         >");
		console.log("speed down:       <");
		console.log("normal speed:     n");
		//console.dir("video: ", video)
	}
	      
	
	// TODO --- see if you can inject a css from elsewhere instead of having to write all the style attributes
	function createSpeedView () { 
		// createElementType( type, parentId, elementId, attributes object {}, insertBefore = false default
		var parentId = v.parentNode.parentNode.id;
		var speedContainerAttributes = {
			 "class": "ytt_speed",
			 "style": ` width:100px; 
						height:30px;
						background:red;
						font-size: 1.5em;           
			            padding: 10px 0px 0px 10px;
		                font-weight: bold;
						position: absolute;
						z-index: 10000
  					 `			    			    			    
		}
		
		speedContainer = Utils.DOM.createElementType("div", parentId, "yttSpeed", speedContainerAttributes, true);
		updateSpeedView();
	}  
	     
	// TODO --- change the visibility by changing a class
	function hideSpeedView () { 
		speedContainer.style.visibility = "hidden" 		
	} 
	
	function showSpeedView () { 
		speedContainer.style.visibility = "visible"
	}
	
	function updateSpeedView () { 
		 // if the element doesn't exist first create it
		 if (speedContainer == undefined) createSpeedView();
		
		 speedContainer.innerHTML = "speed x" + getVideoSpeed();
		
		 var t = speedContainerTimer
		 if ( t != undefined) {
			//there is a timer going so stop it before launching the next one
			clearTimeout(t)
		} 
		
		// show it
		showSpeedView()
		
		//start the new timer that will hide it
		speedContainerTimer = setTimeout(hideSpeedView, speedViewDuration)  		
	}
	
	
  


    //////////////////////////////////////////////   YTT ACCESSIBLE Object
	
	// initialize it and expose it to the window so that you can control it from the console	
	function YTT () { 
		this.video = v;
		this.duration = durationString;
		this.getSpeed = function() { v.playbackRate };
		this.setSpeed = function(speed) { setVideoSpeed(speed) }
		this.resetSpeed = speedNormal;
		
		// IMPORTANT: now that we've created it - let's initialize it
		_initYTT()
	}
	window.YTT = new YTT();
	
	
	
	//////////////////////////////////////////////
	//////////////////////////////////////////////
	
	Utils = {};  
	Utils.DOM = {
		
		// simple class switcher on a t target dom element (jquery)
	    // if it has c1 it will put the c2 and viceversa
	    // example usage if it currently has the pause button > switch to the play button and viceversa
	    toggleClasses : function(t, c1, c2){
	        // you need to use this outside boolean because if you switch it and then test it the boolean will have changed
	        var bool = (t.hasClass(c2)) ? true : false;
	        t.toggleClass(c2, !bool);
	        t.toggleClass(c1, bool);
	    },

	    /**
	     * Utility to inject in the head element some Css or js
	     * it checks if it already exists and removes the previous version, so that if a user reloads the page doesn't copy it many times
	     * @param tagType   :String "css" or "js"
	     * @param tagId     :String the id of the tag to create and remove
	     * @param filePath  :String file to inject
	    */
	    injectInHead: function (tagType, tagId, filePath) {
	        if(tagType == "css") {
	            //remove and inject the new one
	            $("head #"+ tagId).remove();
	            $("head").append($("<link rel='stylesheet' id='" + tagId + "' href='" + filePath + "' type='text/css' media='screen' />"));

	        } else if(tagType == "js") {
	            //TODO implement the injection of JS tag if you need it
	        }
	    },
	     
	
		// create any element ("span", "div", or other)
		createElementType : function (elementType , parentId, newId, attributes, insertBefore = false) {
		    var el = document.createElement(elementType);
		    el.id = newId;

		    //add all the other attributes, ie class, style, data object, anything custom
		    if (attributes) {
		        for (k in attributes) {
		            el.setAttribute(k, attributes[k]); 
		        }
		    }
		    //insert it at the beginning or end of the list
		    var p = document.getElementById(parentId);
		    if (insertBefore) p.insertBefore(el, p.firstChild);
		    else p.appendChild(el);

		    return el;
		},
		
	}
	
})( videoObj )
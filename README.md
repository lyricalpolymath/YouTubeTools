# You Tube Tools
YouTubeTools adds custom tools to the YouTube video playback

----

#### TOOLS
- Change video speed shortcut (">" to increase speed, "<" to lower the speed)

other tools coming soon!



---

#### INSTALL AND USE THE YTT BOOKMARKLET

To install the bookmarklet you have 2 options:

**simple version**  
open the [bookmarklet page](https://lyricalpolymath.gitlab.io/YouTubeTools/index.html) and follow the instructions

OR

**create the bookmarklet by hand**  

- Drag this link [YTTools](#) to the bookmark bar
- copy the following code

```
javascript: (function() {function getRandomInt(min, max) {  return Math.floor(Math.random() * (max - min)) + min;}function injectScript(url, success) { var script = document.createElement('script');script.type='text/javascript'; script.src = url + '?' + getRandomInt(10000, 20000);   console.log('script.src: ' + script.src);var head = document.getElementsByTagName('head')[0], done = false;script.onload = script.onreadystatechange = function() { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { done = true; success(); script.onload = script.onreadystatechange = null;}};        head.appendChild(script);}injectScript('https://lyricalpolymath.gitlab.io/YouTubeTools/YTT.js', function(){});})();
```

- right click on the newly created bookmark
- paste the code into the link field and save it


### !! IMPORTANT !!

In order to serve the YTT.js file to Youtube via a bookmarklet

- it needs to be served through an https url
- you need to **first "host" the file as a normal js file of a static website**.  
if you try to inject it by using the raw version of githubs or gitlabs url, **you will get a Warning that the mime type of the file is "plain/text"** and it can't be executed.


To avoid these problems you can either

1. (GITLAB) create a gitlab page and serve it from there
2. (GITHUB) access github's raw file from a special server

read the instructions in the [bookmarklet.full.js](https://lyricalpolymath.gitlab.io/YouTubeTools/bookmarklet.full.js) file to see how to do this yourself




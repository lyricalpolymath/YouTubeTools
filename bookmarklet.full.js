/*
YTT Bookmarklet - Full version with comments

In order to serve the YTT.js file to Youtube via a bookmarklet
you need to first "host" the file as a normal js file of a static website.
if you try to inject it by using the raw version of githubs or gitlabs url, you will get a Warning that the mime type of the file is "plain/text"

to avoid this problem you can either
1- (GITLAB) create a gitlab page and serve it from there
2- (GITHUB) access githubs raw file from a special server

----------------------------------------------------------
OPTION 1 - create a custom gitlab static page
----------------------------------------------------------
Tutorial and reference: https://about.gitlab.com/2016/04/07/gitlab-pages-setup/


1- create a .gitlab-ci.yml file in the root of your repository
2- copy paste the following contents you have in the file

pages:
  stage: deploy
  script:
  - mkdir .public
  - cp -r * .public
  - mv .public public
  artifacts:
    paths:
    - public
  only:
  - master

3- commit and deploy it to the gitlab server.

4- monitor in Pipelines > Build
    https://gitlab.com/lyricalpolymath/YouTubeTools/builds
    to see when the build is done 
	(Warning it can take up to several minutes - don't worry if you see it "pending" for a long time)

5- when the build is done you can also access the gitlab-pages settings ((https://gitlab.com/lyricalpolymath/YouTubeTools/pages))
that will tell you
"Congratulations! Your pages are served under:
https://lyricalpolymath.gitlab.io/YouTubeTools"

Therefore your .js file will be accessible from
https://lyricalpolymath.gitlab.io/YouTubeTools/YTT.js
or abstracting
https://gitlabUserName.gitlab.io/repoName/fileName.js  
                                

6- Once you've uploaded the page the first time you can modify the js file at will (as well as the html)
and as long as you don't change it of position or directory, the new file will be reachable at the same link
(once the deploy and build is finalized);



----------------------------------------------------------
OPTION 2 - access githubs raw file from a special server
----------------------------------------------------------
http://stackoverflow.com/questions/17341122/link-and-execute-external-javascript-file-hosted-on-github
I haven't tested it yet

-----------------------------------------------------------------------------------

TO TEST CREATE THE BOOKMARKLET WITH THESE
http://mrcoles.com/bookmarklet/
http://www.dev-hq.net/posts/1--create-javascript-bookmarklet

*/

// create a bookmark
// edit it
// copy and paste the following code

javascript: (function() {function getRandomInt(min, max) {  return Math.floor(Math.random() * (max - min)) + min;}function injectScript(url, success) {        var script = document.createElement('script');        script.src = url + "?" + getRandomInt(10000, 20000);   console.log("script.src: " + script.src);var head = document.getElementsByTagName('head')[0],            done = false;        script.onload = script.onreadystatechange = function() { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) {                done = true;                success();                script.onload = script.onreadystatechange = null;}}; head.appendChild(script); }injectScript('https://lyricalpolymath.gitlab.io/YouTubeTools/YTT.js', function(){});})();
